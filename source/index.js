"use strict";

const path = require("path");
const functionFactory = require(path.resolve(__dirname, "functionFactory", "functionFactory.js"));
const encrypt = require(path.resolve(__dirname, "encrypt", "encrypt.js"));
const decrypt = require(path.resolve(__dirname, "decrypt", "decrypt.js"));
const hash = require(path.resolve(__dirname, "hash", "hash.js"));
const hmac = require(path.resolve(__dirname, "hmac", "hmac.js"));
const randomBytes = require(path.resolve(__dirname, "randomBytes", "randomBytes.js"));

exports.defaults = {
    encryptionAlgorithm: "aes192",
    hashingAlgorithm: "sha256",
    key: "270f0cac-71b7-442b-8e3e-81b211acc8eb",
    encoding: "base64"
};

exports.encrypt = functionFactory.getAsyncFunction(functionFactory.getOptionsFunction("data", encrypt.bind(exports)));
exports.encryptSync = functionFactory.getOptionsFunction("data", encrypt.bind(exports));

exports.decrypt = functionFactory.getAsyncFunction(functionFactory.getOptionsFunction("data", decrypt.bind(exports)));
exports.decryptSync = functionFactory.getOptionsFunction("data", decrypt.bind(exports));

exports.hash = functionFactory.getAsyncFunction(functionFactory.getOptionsFunction("data", hash.bind(exports)));
exports.hashSync = functionFactory.getOptionsFunction("data", hash.bind(exports));

exports.hmac = functionFactory.getAsyncFunction(functionFactory.getOptionsFunction("data", hmac.bind(exports)));
exports.hmacSync = functionFactory.getOptionsFunction("data", hmac.bind(exports));

exports.randomBytes = functionFactory.getAsyncFunction(functionFactory.getOptionsFunction("size", randomBytes.bind(exports)));
exports.randomBytesSync = functionFactory.getOptionsFunction("size", randomBytes.bind(exports));
