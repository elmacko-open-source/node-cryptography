"use strict";

const expect = require("chai").expect;
const when = require("when");
const path = require("path");
const cryptography = require(path.resolve("source"));

describe("randomBytes", function()
{
    it("with default settings", function()
    {
        return cryptography.randomBytes(100)
        .then(function(randomBytes)
        {
            expect(randomBytes).to.be.a("string").of.length(136);
        });
    });

    it("with overridden settings", function()
    {
        return cryptography.randomBytes({
            size: 100,
            encoding: "ascii"
        })
        .then(function(randomBytes)
        {
            expect(randomBytes).to.be.a("string").of.length(100);
        });
    });

    it("with callback", function(done)
    {
        cryptography.randomBytes({
            size: 100,
            encoding: "hex",
            callback: function(error, randomBytes)
            {
                expect(error).to.be.null;
                expect(randomBytes).to.be.a("string").of.length(200);
                done();
            }
        });
    });

    it("synchronous", function()
    {
        expect(cryptography.randomBytesSync(100)).to.be.a("string").of.length(136);
    });

    it("promise error", function(done)
    {
        cryptography.randomBytes({})
        .then(function()
        {
            done(new Error("Resolved when it should have rejected"));
        })
        .catch(function(error)
        {
            done();
        });
    });

    it("callback error", function(done)
    {
        cryptography.randomBytes({
            callback: function(error)
            {
                expect(error).to.be.an("error");
                done();
            }
        });
    });

    it("synchronous error", function()
    {
        expect(cryptography.randomBytesSync.bind(cryptography, {})).to.throw;
    });
});
