"use strict";

const crypto = require("crypto");

module.exports = function(options)
{
    let random = crypto.randomBytes(options.size);

    return random.toString(options.encoding || this.defaults.encoding);
};
