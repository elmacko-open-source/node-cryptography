"use strict";

const expect = require("chai").expect;
const when = require("when");
const path = require("path");
const cryptography = require(path.resolve("source"));

describe("hmac", function()
{
    it("with default settings", function()
    {
        return cryptography.hmac("test")
        .then(function(hmac)
        {
            expect(hmac).to.equal("C8jL12Tsae6Mmp1aARY36gtNa9wFy6w+WRKWCqoBllQ=");
        });
    });

    it("with overridden settings", function()
    {
        return cryptography.hmac({
            data: "test",
            algorithm: "sha1",
            key: "test",
            encoding: "hex"
        })
        .then(function(hmac)
        {
            expect(hmac).to.equal("0c94515c15e5095b8a87a50ba0df3bf38ed05fe6");
        });
    });

    it("with callback", function(done)
    {
        cryptography.hmac({
            data: "test",
            callback: function(error, hmac)
            {
                expect(error).to.be.null;
                expect(hmac).to.equal("C8jL12Tsae6Mmp1aARY36gtNa9wFy6w+WRKWCqoBllQ=");
                done();
            }
        });
    });

    it("synchronous", function()
    {
        expect(cryptography.hmacSync("test")).to.equal("C8jL12Tsae6Mmp1aARY36gtNa9wFy6w+WRKWCqoBllQ=");
    });

    it("promise error", function(done)
    {
        cryptography.hmac({})
        .then(function()
        {
            done(new Error("Resolved when it should have rejected"));
        })
        .catch(function(error)
        {
            done();
        });
    });

    it("callback error", function(done)
    {
        cryptography.hmac({
            callback: function(error)
            {
                expect(error).to.be.an("error");
                done();
            }
        });
    });

    it("synchronous error", function()
    {
        expect(cryptography.hmacSync.bind(cryptography, {})).to.throw;
    });
});
