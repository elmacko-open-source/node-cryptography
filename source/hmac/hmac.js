"use strict";

const crypto = require("crypto");

module.exports = function(options)
{
    let hmac = crypto.createHmac(
        options.algorithm || this.defaults.hashingAlgorithm,
        options.key || this.defaults.key
    );

    hmac.update(
        options.data,
        "utf8"
    );

    return hmac.digest(options.encoding || this.defaults.encoding);
};
