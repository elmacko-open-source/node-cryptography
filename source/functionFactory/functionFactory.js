"use strict";

const lodash = require("lodash");
const when = require("when");

exports.getOptionsFunction = function(name, call)
{
    return function(options)
    {
        if(!lodash.isObject(options))
        {
            let defaultOptions = {};
            defaultOptions[name] = options;
            options = defaultOptions;
        }

        return call(options);
    };
};

exports.getAsyncFunction = function(call)
{
    let self = this;

    return function(options)
    {
        if(!lodash.isFunction(options.callback))
        {
            return self.promiseFunction(options, call);
        }

        self.callbackFunction(options, call);
    }
};

exports.callbackFunction = function(options, call)
{
    try
    {
        let results = call(options);

        options.callback(null, results);
    }
    catch(error)
    {
        options.callback(error);
    }
};

exports.promiseFunction = function(options, call)
{
    try
    {
        let results = call(options);

        return when.resolve(results);
    }
    catch(error)
    {
        return when.reject(error);
    }
};
