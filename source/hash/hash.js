"use strict";

const crypto = require("crypto");

module.exports = function(options)
{
    let hash = crypto.createHash(options.algorithm || this.defaults.hashingAlgorithm);

    hash.update(
        options.data,
        "utf8"
    );

    return hash.digest(options.encoding || this.defaults.encoding);
};
