"use strict";

const expect = require("chai").expect;
const when = require("when");
const path = require("path");
const cryptography = require(path.resolve("source"));

describe("hash", function()
{
    it("with default settings", function()
    {
        return cryptography.hash("test")
        .then(function(hash)
        {
            expect(hash).to.equal("n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is0V1sFbDwCgg=");
        });
    });

    it("with overridden settings", function()
    {
        return cryptography.hash({
            data: "test",
            algorithm: "sha1",
            key: "test",
            encoding: "hex"
        })
        .then(function(hash)
        {
            expect(hash).to.equal("a94a8fe5ccb19ba61c4c0873d391e987982fbbd3");
        });
    });

    it("with callback", function(done)
    {
        cryptography.hash({
            data: "test",
            callback: function(error, hash)
            {
                expect(error).to.be.null;
                expect(hash).to.equal("n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is0V1sFbDwCgg=");
                done();
            }
        });
    });

    it("synchronous", function()
    {
        expect(cryptography.hashSync("test")).to.equal("n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is0V1sFbDwCgg=");
    });

    it("promise error", function(done)
    {
        cryptography.hash({})
        .then(function()
        {
            done(new Error("Resolved when it should have rejected"));
        })
        .catch(function(error)
        {
            done();
        });
    });

    it("callback error", function(done)
    {
        cryptography.hash({
            callback: function(error)
            {
                expect(error).to.be.an("error");
                done();
            }
        });
    });

    it("synchronous error", function()
    {
        expect(cryptography.hashSync.bind(cryptography, {})).to.throw;
    });
});
