"use strict";

const crypto = require("crypto");

module.exports = function(options)
{
    let decipher = crypto.createDecipher(
        options.algorithm || this.defaults.encryptionAlgorithm,
        options.key || this.defaults.key
    );
    let decrypted = decipher.update(
        options.data,
        options.encoding || this.defaults.encoding,
        "utf8"
    );

    decrypted += decipher.final("utf8");

    return decrypted;
};
