"use strict";

const expect = require("chai").expect;
const when = require("when");
const path = require("path");
const cryptography = require(path.resolve("source"));

describe("decrypt", function()
{
    it("with default settings", function()
    {
        return cryptography.decrypt("vt6iRrKQlTMsYoSkmebB+w==")
        .then(function(decrypted)
        {
            expect(decrypted).to.equal("test");
        });
    });

    it("with overridden settings", function()
    {
        return cryptography.decrypt({
            data: "ffe301af",
            algorithm: "RC2-CFB",
            key: "test",
            encoding: "hex"
        })
        .then(function(decrypted)
        {
            expect(decrypted).to.equal("test");
        });
    });

    it("with callback", function(done)
    {
        cryptography.decrypt({
            data: "vt6iRrKQlTMsYoSkmebB+w==",
            callback: function(error, decrypted)
            {
                expect(error).to.be.null;
                expect(decrypted).to.equal("test");
                done();
            }
        });
    });

    it("synchronous", function()
    {
        expect(cryptography.decryptSync("vt6iRrKQlTMsYoSkmebB+w==")).to.equal("test");
    });

    it("promise error", function(done)
    {
        cryptography.decrypt({})
        .then(function()
        {
            done(new Error("Resolved when it should have rejected"));
        })
        .catch(function(error)
        {
            done();
        });
    });

    it("callback error", function(done)
    {
        cryptography.decrypt({
            callback: function(error)
            {
                expect(error).to.be.an("error");
                done();
            }
        });
    });

    it("synchronous error", function()
    {
        expect(cryptography.decryptSync.bind(cryptography, {})).to.throw;
    });
});
