"use strict";

const expect = require("chai").expect;
const when = require("when");
const path = require("path");
const cryptography = require(path.resolve("source"));

describe("encrypt", function()
{
    it("with default settings", function()
    {
        return cryptography.encrypt("test")
        .then(function(encrypted)
        {
            expect(encrypted).to.equal("vt6iRrKQlTMsYoSkmebB+w==");
        });
    });

    it("with overridden settings", function()
    {
        return cryptography.encrypt({
            data: "test",
            algorithm: "RC2-CFB",
            key: "test",
            encoding: "hex"
        })
        .then(function(encrypted)
        {
            expect(encrypted).to.equal("ffe301af");
        });
    });

    it("with callback", function(done)
    {
        cryptography.encrypt({
            data: "test",
            callback: function(error, encrypted)
            {
                expect(error).to.be.null;
                expect(encrypted).to.equal("vt6iRrKQlTMsYoSkmebB+w==");
                done();
            }
        });
    });

    it("synchronous", function()
    {
        expect(cryptography.encryptSync("test")).to.equal("vt6iRrKQlTMsYoSkmebB+w==");
    });

    it("promise error", function(done)
    {
        cryptography.encrypt({})
        .then(function()
        {
            done(new Error("Resolved when it should have rejected"));
        })
        .catch(function(error)
        {
            done();
        });
    });

    it("callback error", function(done)
    {
        cryptography.encrypt({
            callback: function(error)
            {
                expect(error).to.be.an("error");
                done();
            }
        });
    });

    it("synchronous error", function()
    {
        expect(cryptography.encryptSync.bind(cryptography, {})).to.throw;
    });
});
