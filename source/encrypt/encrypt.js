"use strict";

const crypto = require("crypto");

module.exports = function(options)
{
    let cipher = crypto.createCipher(
        options.algorithm || this.defaults.encryptionAlgorithm,
        options.key || this.defaults.key
    );
    let encrypted = cipher.update(
        options.data,
        "utf8",
        options.encoding || this.defaults.encoding
    );

    encrypted += cipher.final(options.encoding || this.defaults.encoding);

    return encrypted;
};
