# node-cryptography
[![npm version](https://badge.fury.io/js/cryptography.svg)](https://badge.fury.io/js/cryptography)
[![coverage report](https://gitlab.com/elmacko-open-source/node-cryptography/badges/master/coverage.svg)](https://gitlab.com/elmacko-open-source/node-cryptography/commits/master)
[![David](https://img.shields.io/david/jnssnmrcs/node-cryptography.svg)](https://gitlab.com/elmacko-open-source/node-cryptography)
[![David Dev](https://img.shields.io/david/dev/jnssnmrcs/node-cryptography.svg)](https://gitlab.com/elmacko-open-source/node-cryptography)
[![npm](https://img.shields.io/npm/dt/cryptography.svg)](https://www.npmjs.com/package/cryptography)
[![Pipeline Status](https://gitlab.com/elmacko-open-source/node-cryptography/badges/master/build.svg)](https://gitlab.com/elmacko-open-source/node-cryptography/pipelines)
[![Code Climate](https://img.shields.io/codeclimate/maintainability/jnssnmrcs/node-cryptography.svg)](https://codeclimate.com/github/jnssnmrcs/node-cryptography)
[![License](https://img.shields.io/npm/l/cryptography.svg)](https://gitlab.com/elmacko-open-source/node-cryptography/blob/master/LICENSE)

## About
Simplified functions for nodejs cryptography.

## Usage

### Functions

#### cryptography.encrypt(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.callback` [`<Function>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)
    * `error` [`<Error>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error)
    * `encrypted` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Returns a promise which is resolved with encrypted string. If a callback is passed it is called with encrypted string.

#### cryptography.encryptSync(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Synchronous encrypt(). Returns encrypted string.

#### cryptography.decrypt(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.callback` [`<Function>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)
    * `error` [`<Error>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error)
    * `decrypted` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Returns a promise which is resolved with decrypted string. If a callback is passed it is called with decrypted string.

#### cryptography.decryptSync(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Synchronous decrypt(). Returns decrypted string.

#### cryptography.hmac(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.callback` [`<Function>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)
    * `error` [`<Error>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error)
    * `hashed` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Returns a promise which is resolved with hashed string. If a callback is passed it is called with hashed string.

#### cryptography.hmacSync(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Synchronous hmac(). Returns hashed string.

#### cryptography.hash(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.callback` [`<Function>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)
    * `error` [`<Error>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error)
    * `hashed` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Returns a promise which is resolved with hashed string. If a callback is passed it is called with hashed string.

#### cryptography.hashSync(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.key` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.algorithm` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.data` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Synchronous hash(). Returns hashed string.

#### cryptography.randomBytes(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<Number>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Number_type)
 * `options.size` [`<Number>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Number_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)
 * `options.callback` [`<Function>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)
    * `error` [`<Error>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error)
    * `randomBytes` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Returns a promise which is resolved with random bytes string. If a callback is passed it is called with random bytes string.

#### cryptography.randomBytesSync(options)
 * `options` [`<Object>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) | [`<Number>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Number_type)
 * `options.size` [`<Number>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Number_type)
 * `options.encoding` [`<string>`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type)

Synchronous randomBytes(). Returns random bytes string.

### Example with promise
```javascript
const cryptography = require("cryptography");

// Set defaults
cryptography.defaults.key = "password";
cryptography.defaults.encryptionAlgorithm = "aes192";
cryptography.defaults.encoding = "hex";

// Use defaults
cryptography.encrypt("string to encrypt")
.then(function(encrypted)
{
    return cryptography.decrypt(encrypted);
})
.then(function(decrypted)
{
    console.log(decrypted) // outputs: "string to encrypt"
})
.catch(function(error)
{
    // An error occurred
});

// Override defaults for one call
cryptography.encrypt({
    key: "another password",
    data: "string to encrypt"
})
.then(function(encrypted)
{
    return cryptography.decrypt({
        key: "another password",
        data: encrypted
    });
})
.then(function(decrypted)
{
    console.log(decrypted) // outputs: "string to encrypt"
})
.catch(function(error)
{
    // An error occurred
});
```

### Example with callback
```javascript
const cryptography = require("cryptography");
var decryptCallback = function(error, decrypted)
{
    if(error)
    {
        // Error occurred
        return;
    }

    console.log(decrypted);
};
var encryptCallback = function(error, encrypted)
{
    if(error)
    {
        // Error occurred
        return;
    }

    cryptography.decrypt({
        data: encrypted,
        callback: decryptCallback
    });
};

cryptography.encrypt({
    data: "string to encrypt",
    callback: encryptCallback
});
```

### Example with synchronous
```javascript
const cryptography = require("cryptography");

try
{
    var encrypted = cryptography.encryptSync("string to encrypt");
    var decrypted = cryptography.decryptSync(encrypted);
}
catch(error)
{
    // Error occurred
    return;
}

console.log(decrypted);
```
